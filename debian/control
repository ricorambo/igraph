Source: igraph
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               cmake,
               d-shlibs,
               bison,
               flex,
               libxml2-dev,
               libplfit-dev,
               libgmp-dev,
               libarpack2-dev,
               libblas-dev,
               liblapack-dev,
               libglpk-dev,
               libf2c2-dev,
               liblzma-dev
Build-Depends-Indep: python3,
                     xmlto,
                     source-highlight,
                     fop,
                     fonts-font-awesome,
                     libxml2-utils,
                     docbook2x,
                     texinfo
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/igraph
Vcs-Git: https://salsa.debian.org/med-team/igraph.git
Homepage: https://igraph.org/c/
Rules-Requires-Root: no

Package: libigraph3t64
Provides: ${t64:Provides}
Replaces: libigraph3
Breaks: libigraph3 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: library for creating and manipulating graphs
 igraph is a library for creating and manipulating graphs.
 It is intended to be as powerful (ie. fast) as possible to enable the
 analysis of large graphs.
 .
 This is the runtime library package.

Package: libigraph-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libigraph3t64 (= ${binary:Version}),
         libxml2-dev,
         ${devlibs:Depends},
         ${misc:Depends}
Suggests: libigraph-doc,
          pkgconf
Description: library for creating and manipulating graphs - development files
 igraph is a library for creating and manipulating graphs.
 It is intended to be as powerful (ie. fast) as possible to enable the
 analysis of large graphs.
 .
 This package contains the include files and static library for the igraph
 C library.

Package: libigraph-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: fonts-font-awesome,
         ${misc:Depends}
Suggests: libigraph-dev (= ${binary:Version}),
          libsuitesparse-dev,
          www-browser
Breaks: libigraph-examples (<< 0.9)
Replaces: libigraph-examples (<< 0.9)
Description: library for creating and manipulating graphs - reference manual
 igraph is a library for creating and manipulating graphs.
 It is intended to be as powerful (ie. fast) as possible to enable the
 analysis of large graphs.
 .
 This package provides the reference manual for the igraph C library;
 it also contains examples and tutorial material.

Package: libigraph-examples
Architecture: all
Multi-Arch: foreign
Section: oldlibs
Depends: libigraph-doc,
         ${misc:Depends}
Description: transitional package
 This is a transitional package. It can safely be removed.
