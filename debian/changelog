igraph (0.10.15+ds-2) unstable; urgency=medium

  * Debianization:
    - d/rules:
      - DEB_BUILD_OPTIONS, disable LTO build (Closes: #1092834);
    - d/adhoc/examples/simple/Makefile, hardden (thanks to Matthias
      Klose <matthias.klose@canonical.com> for reporting the issue);
    - various minor janitorial changes.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 18 Jan 2025 23:34:52 +0000

igraph (0.10.15+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/libigraph3t64.symbols, update;
    - d/patches/*:
      - d/p/upstream-silence-tests-benchmarks-*.patch, integrated;
    - d/control:
      - Standards-Version, dump to 4.7.0 (no change);
    - d/libigraph-doc.docs, refresh;

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 10 Nov 2024 04:24:30 +0000

igraph (0.10.13+ds-2) unstable; urgency=medium

  * Debianization:
    - d/patches/*:
      - -d/p/upstream-fix-multiarch-file_conflict.patch, correct
         (thanks to Szabolcs Horvat <szhorvat@gmail.com> for the correction).

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 15 Jul 2024 21:11:34 +0000

igraph (0.10.13+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/libigraph3t64.symbols, update;
    - d/patches/*:
      - d/p/upstream-silence-tests-benchmarks-{format_truncation,
        unused_but_set_variable}.patch, introduce;
    - various minor janitorial changes.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 28 Jun 2024 20:51:42 +0000

igraph (0.10.12+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/libigraph3t64.symbols, update.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 09 May 2024 18:27:31 +0000

igraph (0.10.11+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/libigraph3t64.symbols, update;
    - d/libigraph-doc.docs, add CONTRIBUTORS.txt.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 03 Apr 2024 10:29:04 +0000

igraph (0.10.10+ds-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062443

 -- Lukas Märdian <slyon@debian.org>  Wed, 28 Feb 2024 10:59:17 +0000

igraph (0.10.10+ds-1) unstable; urgency=medium

  [ Aymeric Agon-Rambosson ]
  * Update file references inside info file to match rename

  [ Andreas Tille ]
  * New upstream version
  * Update symbols file

 -- Andreas Tille <tille@debian.org>  Tue, 13 Feb 2024 20:11:19 +0100

igraph (0.10.9+ds-2) unstable; urgency=medium

  * FTBFS fix release, igraph_hrg test not fully tested by upstream.
  * Debianization:
    - d/patches/*:
      - d/p/debianization-tests-igraph_hrg-freeze.patch, introduce
        (Closes: #1063607).

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 10 Feb 2024 21:40:27 +0000

igraph (0.10.9+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/libigraph3.symbols, update;
    - info document, reintroduce;
    - various janitorial changes.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 04 Feb 2024 23:18:34 +0000

igraph (0.10.8+ds-2) unstable; urgency=medium

  * As per https://github.com/igraph/igraph/issues/2450 we ignore random
    failures on mips64el
    Closes: #970497
  * No tab in license text (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 11 Dec 2023 13:21:53 +0100

igraph (0.10.8+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/libigraph3.symbols, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 18 Nov 2023 12:01:59 +0000

igraph (0.10.7+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/libigraph3.symbols, update.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 08 Sep 2023 19:55:48 +0000

igraph (0.10.6+ds-1) unstable; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/patches/*:
      - d/p/upstream-fix-multiarch-file_conflict.patch, introduce;
    - d/libigraph-dev.install, fix multiarch issue (see previous patch);
    - d/libigraph3.symbols, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 15 Jul 2023 15:31:02 +0000

igraph (0.10.4+ds-2) unstable; urgency=medium

  * Upload to unstable.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 01 Jul 2023 21:46:28 +0000

igraph (0.10.4+ds-1) experimental; urgency=medium

  * New upstream micro version.
  * Debianization:
    - d/p/patches/*:
      - d/p/upstream-fixed-benchmarks-ig_distances-RNG-explicit_order.patch,
        obsoleted;
    - d/control:
      - Standards-Version, dump to 4.6.2 (no change);
    - d/libigraph3.symbols, update;
    - d/rules:
      - override_dh_auto_test-arch target, build test executables
        before calling dh_auto_test (see #924052);
    - various janitorial changes.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 28 Mar 2023 11:49:42 +0000

igraph (0.10.2+ds-2) unstable; urgency=medium

  * Upload to unstable.
  * Debianization:
    - migrate from pkg-config to pkgconf, affect d/{,t/}/control;
    - libigraph-dev Depends now on libxml2-dev;
    - d/p//upstream-fixed-benchmarks-ig_distances-RNG-explicit_order.patch,
      import from upstream.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 20 Nov 2022 13:28:36 +0000

igraph (0.10.2+ds-1) experimental; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/watch, now use GitHub API to avoid breaking change from GitHub web UI
      (see https://lists.debian.org/debian-devel/2022/09/msg00229.html);
    - d/libigraph3.symbols, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 29 Oct 2022 14:23:46 +0000

igraph (0.10.1+ds-1) experimental; urgency=medium

  * New upstream minor (official release) version with breaking changes.
  * Debianization:
    - d/patches/*:
      - d/p/upstream-fix-*.patch, integrated;
    - d/libigraph3.symbols, update;
    - d/s/lintian-overrides, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 10 Sep 2022 17:14:25 +0000

igraph (0.10.0~rc2+ds-1) experimental; urgency=medium

  * New upstream minor (release candidate 2) version with breaking changes.
  * Debianization:
    - d/patches/*:
      - d/p/upstream-{fix-examples-nonewlineatendofoutfile,
          enhancement-examples-stdwar, source-lintian-spelling,
          fix-doc-cmake, missing-tools}.patch, integrated;
      - d/p/upstream-fix-{balling_manoeuver_issue,
         examples-simple-safelocale-typo}.patch, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 17 Aug 2022 18:26:52 +0000

igraph (0.10.0~rc1+ds-2) experimental; urgency=medium

  * Debianization:
    - d/patches/*:
      - d/p/debianization-external-plfit-neutralize-future_code.patch,
        obsoleted;
    - d/control:
      - Build-Depends field, cleanup.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 10 Aug 2022 13:17:49 +0000

igraph (0.10.0~rc1+ds-1) experimental; urgency=medium

  * New upstream minor (release candidate) version with breaking changes.
  * Debianization:
    - SONAME, bump from 2 to 3;
    - d/watch, adapt;
    - d/copyright:
      - Files-Excluded, keep the now tainted vendor/cs material;
      - copyright entries, adapt;
    - d/patches/*:
      - d/p/upstream-{fix-examples-nonewlineatendofoutfile,
          enhancement-examples-stdwar, source-lintian-spelling,
          fix-doc-cmake, missing-tools}.patch, introduce;
      - d/p/debianization-suitesparse-header_folder.patch, obsoleted;
      - d/p/debianization.patch, refresh;
      - d/p/debianization-external-plfit-neutralize-future_code, introduce;
    - tests/control:
      - simple check, operate now in a locales-all environment;
    - d/rules, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 07 Aug 2022 14:52:48 +0000

igraph (0.9.10+ds-2) unstable; urgency=medium

  * Debianization:
    - d/watch, now use GitHub API to avoid breaking change from GitHub web UI
      (see https://lists.debian.org/debian-devel/2022/09/msg00229.html).

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 29 Oct 2022 13:15:30 +0000

igraph (0.9.10+ds-1) unstable; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/libigraph2.symbols, update;
    - d/control:
      - Standards-Version, dump to 4.6.1 (no change);
    - d/patches/*:
      - d/p/upstream-source-lintian-spelling.patch, imported
        from experimental.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 10 Sep 2022 11:37:09 +0000

igraph (0.9.9+ds-1) unstable; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/watch, now download directly from github.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 04 Jun 2022 12:45:17 +0000

igraph (0.9.8+ds-1) unstable; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/libigraph2.symbols, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 09 Apr 2022 13:45:34 +0000

igraph (0.9.7+ds-1) unstable; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/rules:
      - explicitly disable LTO for the static library as LTO does
        not really achieve anything for static libraries (thanks to
        Timo Röhling <roehling@debian.org> for the hint).
    - d/patches/*:
      - d/p/upstream-external-plfit.patch, update (partially integrated).

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 16 Mar 2022 23:11:57 +0000

igraph (0.9.6+ds-2) unstable; urgency=medium

  * Source only upload
  * Add missing build dependency on dh addon.
  * Add two missing symbols in symbols file

 -- Andreas Tille <tille@debian.org>  Sat, 08 Jan 2022 21:50:51 +0100

igraph (0.9.6+ds-1) unstable; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/aptches/*:
      - d/p/debianization{,-suitesparse-header_folder}.patch, update;
      - d/p/upstream-external-plfit.patch, refresh;
      - d/p/8c5711d8d143a40602dbd177f1a343471304efa7.patch, obsoleted.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 06 Jan 2022 09:29:26 +0000

igraph (0.9.5+ds-3) unstable; urgency=medium

  * Debianization:
    - d/copyright, update copyright year tuples;
    - d/adhoc/examples/*/Makfile, idem;
    - d/patches/*:
      - d/p/upstream-external-plfit.patch, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 04 Jan 2022 20:46:03 +0000

igraph (0.9.5+ds-2) unstable; urgency=medium

  * Fix watchfile (comment) to detect new versions on github
  * cme fix dpkg-control
  * Fix test suite on i386 (see upstream issue #1894)

 -- Andreas Tille <tille@debian.org>  Sun, 19 Dec 2021 08:28:23 +0100

igraph (0.9.5+ds-1) experimental; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/copyright:
      - Files-Excluded list, now discard vendor/plfit;
    - d/control:
      - Build-Depends field, add libplfit-dev;
    - d/tests/control:
      - Depends field for benchmarks test, add libplfit-dev;
    - d/rules:
      - override_dh_install-indep target, update;
      - override_dh_fixterm target, obsoleted;
    - d/patches/upstream-external-plfit.patch, introduce;
    - d/adhoc/adhoc/examples/{simple,tutorial}/Makefile,
        add option -Wno-maybe-uninitialized to CFLAGS;
    - d/libigraph-doc.docs, introduce;
    - d/libigraph2.symbols, refresh;
    - d/README.Debian, obsoleted.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 14 Nov 2021 20:24:31 +0000

igraph (0.9.4+ds-3) experimental; urgency=medium

  * Debianization:
    - d/control:
      - Build-Depends-Indep, add python3;
    - d/copyright, harden.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 03 Nov 2021 21:04:55 +0000

igraph (0.9.4+ds-2) experimental; urgency=medium

  * FTBFS fix release: the new major upstream version appears to build
    successfully with gcc-11 (Closes: #984058).
  * Debianization:
    - d/rules, fix typo in CONF_FLAGS;
    - d/control:
      - Standards-Version, dump to 4.6.0 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 11 Oct 2021 19:53:51 +0000

igraph (0.9.4+ds-1) experimental; urgency=medium

  * New upstream minor version (minor changes)
  * Debianization;
    - d/copyright:
      - Files-Excluded field, refresh;
    - d/watch, update;
    - d/rules:
      - override_dh_install-indep target, update;
    - d/libigraph-dev.install, install (new) cmake material;
    - d/libigraph-doc.{install,doc-base}, now manage PDF manual;
    - d/libigraph2.symbols, introduce;
    - d/s/lintian-overrides, introduce;
    - d/a/e/b/Makefile, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 18 Jul 2021 11:18:58 +0000

igraph (0.9.1+ds1-1) UNRELEASED; urgency=medium

  * New upstream minor version (major changes)
  * Debianization:
    - d/control:
      - SO version, bump to version 2;
      - Build-Depends-Indep field, introduce;
      - Build-Depends field:
        - cmake, add (migration to cmake);
        - jdupes, discard (no more needed);
      - Package libigraph2:
        - introduce (bumped SO version, see above);
        - Conflicts and Replaces fields, discard;
      - Package libigraph-examples, renamed libigraph-doc;
    - d/copyright:
      - Files-Excluded field, refresh;
      - Debian Files:
        - copyright, add myself;
      - refresh;
    - d/rules:
      - migrate from autotools to cmake;
      - refresh;
    - d/clean, discard;
    - d/tests:
      - d/t/control:
        - internal tests check, discard;
        - tutorial check, introduce;
      - d/t/check, set simple as default;
    - d/patches:
      - d/p/upstream-fix-examples_benchmarks_bench_h-missing-include.patch,
        integrated;
      - d/p/upstream-fix-examples_simple-gcc-warnings.patch, integrated;
      - d/p/upstream-fix-lintian-spelling_error_in_binary.patch, refresh;
      - d/p/upstream-fixed-{git-interfer-nomore,cmake-check,XSOVERSION},patch,
        fixed by and imported from upstream (thanks to upstream team);
      - d/p/debianization{,-doc-local_fonts}.patch, introduce;
      - d/p/soversion.patch, SO version manage via d/p/debianization.patch;
      - d/p/upstream-enhance-external_suitesparse.patch, obsoleted;
      - d/p/do_not_make_anything_in_doc.patch, obsoleted;
      - d/p/enable_removing_third_party_code.patch, obsoleted;
    - d/adhoc/examples/*, refresh;
    - d/libigraph-dev.manpages, introduce;
    - d/libigraph-doc.{install,doc-base}, introduce;
    - d/libigraph1.symbols.amd64, temporarily discard (new and not fixed ABI);
    - d/libigraph-examples.links, obsoleted.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 12 Apr 2021 18:36:28 +0000

igraph (0.8.5+ds1-1) unstable; urgency=medium

  * Discard d/p/skip_tests_accessing_remote.patch as it is now obsolete.
  * Make external SuiteSparse library suite:
    - d/p/upstream-enhance-external_suitesparse.patch ;
    - d/p/debianization-suitesparse-header_folder.patch .
  * Add libsuitesparse-dev to Build-Depends in d/control
    and to Depends in d/t/control (simple tests).
  * Simplify d/rules
  * Uncomment and update d/libigraph{1,-dev}.install .
  * Add to Files-Excluded list: src/{cs,SuiteSparse_config} config.h.in .
  * Update repacksuffix and dversionmangle options in d/watch .
  * Refresh d/libigraph1.symbols.amd64 .

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 26 Dec 2020 15:41:15 +0000

igraph (0.8.5+ds-3) unstable; urgency=medium

  * Add do-nothing override_dh_auto_{build,test,install}-indep targets in
    d/rules to allow arch all build.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 20 Dec 2020 16:29:31 +0000

igraph (0.8.5+ds-2) unstable; urgency=medium

  * Add myself to Uploaders.
  * d/tests based on examples material, introduce.
  * d/adhoc/examples/{tests,simple,benchmarks}/Makefile, introduce
      (ease d/tests).
  * d/patches/*:
    - d/p/upstream-fix-examples*.patch, introduce (ease d/tests);
    - d/p/upstream-fix-lintian-spelling_error_in_binary.patch, introduce.
  * d/rules:
    - override_dh_installchangelogs target: CHANGELOG.md as changelog;
    - override_dh_compress-indep target: examples material is now
       uncompressed (ease d/tests);
    - override_dh_install-indep target, machinery to permit tests over
       examples material (ease d/tests).
  * Add Multi-Arch fields.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 20 Dec 2020 14:40:34 +0000

igraph (0.8.5+ds-1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream version
  * Update Files-Excluded list (clean debclean)
  * Update symbols (d/libigraph1.symbols.amd64)
  * Bump Standards Version to 4.5.1 (no change)
  * Set Forwarded field in patches to not-needed (Debian specific patches)
  * Update d/copyright
  * Add hardening=+all to DEB_BUILD_MAINT_OPTIONS in d/rules
  * Link duplicates in libigraph-examples with jdupes(1)
  * Introduce d/clean

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 13 Dec 2020 19:27:48 +0000

igraph (0.8.3+ds-1) unstable; urgency=medium

  * New upstream version
  * New symbols

 -- Andreas Tille <tille@debian.org>  Sat, 03 Oct 2020 13:07:34 +0200

igraph (0.8.2+ds-2) unstable; urgency=medium

  * debhelper-compat 13 (routine-update)
  * Remove *.la file from debian/tmp to enable dh_missing --fail-missing
  * Fix symbols to build with gcc-10
    Closes: #966963

 -- Andreas Tille <tille@debian.org>  Mon, 03 Aug 2020 17:27:43 +0200

igraph (0.8.2+ds-1) unstable; urgency=medium

  * New upstream version
  * Most patches are applied upstream
  * Point watch file to official download location
    Closes: #958999
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Add new symbol

 -- Andreas Tille <tille@debian.org>  Thu, 30 Apr 2020 10:18:11 +0200

igraph (0.8.1+ds-3) unstable; urgency=medium

  [ Tamás Nepusz ]
  * Another set of upstream patches relaxing tests to enable building on
    all architectures
    Closes: #956821

  [ Andreas Tille ]
  * Provide symbols file only for amd64 to reduce maintenance burden

 -- Andreas Tille <tille@debian.org>  Fri, 24 Apr 2020 10:39:44 +0200

igraph (0.8.1+ds-2) unstable; urgency=medium

  * Drop --with-external-f2c (thanks for the hint to Szabolcs Horvát)

  [ Tamás Nepusz ]
  * Upstream patches relaxing tests to enable building on all architectures

 -- Andreas Tille <tille@debian.org>  Mon, 20 Apr 2020 21:22:20 +0200

igraph (0.8.1+ds-1) unstable; urgency=medium

  * New upstream version
  * Remove third party code that is not used anyway from upstream source
  * Add missings in d/copyright
    Closes: #953941
  * New upstream version
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Update symbols file

 -- Andreas Tille <tille@debian.org>  Mon, 30 Mar 2020 15:02:01 +0200

igraph (0.8.0-1) unstable; urgency=medium

  * New upstream version
  * Point watch file to github
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Call ./bootstrap.sh in dh_autoreconf
  * Build-Depends: bison, flex
  * Add README.source for Rdata files
  * Add symbols file
  * Build-Depends: liblzma-dev
    Closes: #951933
  * Bump soname due to ABI change
  * Use d-shlibs
  * Upstream does not provide texinfo file any more
  * Add example package
  * Fix permissions

 -- Andreas Tille <tille@debian.org>  Sun, 23 Feb 2020 17:04:14 +0100

igraph (0.7.1-4) unstable; urgency=medium

  * use a different starting vector for
    igraph_community_leading_eigenvector() to prevent errors
    with ARPACK 3.6.3 (Thanks for the patch to Tamás Nepusz)
    Closes: #902760
  * Drop Tamás Nepusz <ntamas@gmail.com> from uploaders (Tamás, thanks
    for your previous work on the Debian package and the support you
    provide on upstream code)
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Tue, 15 Jan 2019 15:10:32 +0100

igraph (0.7.1-3) unstable; urgency=medium

  [ Mathieu Malaterre ]
  * Remove self from Uploaders.

  [ Andreas Tille ]
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Versioned Build-Depends: libarpack2-dev (>= 3.6.2-1~) and
    Fixing issues with ARPACK 3.6, related to igraph issue #1107
    Partly deals with bug #902760
  * Exclude tests requiring remote access
  * Secure URI in copyright format
  * Drop useless get-orig-source target
  * Remove trailing whitespace in debian/changelog
  * Apply fix for CVE-2018-20349
    Closes: #917211
  * Ignore remaining build time errors to get fix for CVE-2018-20349 out

 -- Andreas Tille <tille@debian.org>  Mon, 24 Dec 2018 15:25:53 +0100

igraph (0.7.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++5 ABI transition (closes: 791075).

 -- Julien Cristau <jcristau@debian.org>  Sun, 16 Aug 2015 17:36:15 +0200

igraph (0.7.1-2) unstable; urgency=medium

  [ Andreas Tille ]
  * Move packagiong from SVN to Git

  [ Tamas Nepusz ]
  * Remove debian/patches/link_f2c.patch; f2c linkage not needed if
    BLAS, LAPACK and ARPACK are all linked dynamically
    Closes: #702882
  * debian/patches/remove_unused_test_target.patch added to make
    dh_auto_test work
  * debian/patches/fix_failing_tests.patch added to fix some failing
    test cases
  * debian/patches/cppflags_restore.patch added to fix incorrect
    handling of CPPFLAGS in the configure script
  * debian/patches/drl_spelling_fix.patch added to fix a spelling
    error in the source and silence a lintian warning

 -- Tamás Nepusz <ntamas@gmail.com>  Fri, 29 Aug 2014 08:39:02 +0000

igraph (0.7.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 08 Aug 2014 16:12:35 +0200

igraph (0.6.5-5) unstable; urgency=medium

  * use dh-autoreconf instead of autotools-dev (thanks to Logan Rosen
    <logan@ubuntu.com> for the patch)
    Closes: #733712

 -- Andreas Tille <tille@debian.org>  Tue, 31 Dec 2013 08:37:18 +0100

igraph (0.6.5-4) unstable; urgency=medium

  * cme fix dpkg-control
  * debian/patches/link_f2c.patch: Make sure libf2c will be properly linked
    Closes: #702882

 -- Andreas Tille <tille@debian.org>  Thu, 19 Dec 2013 11:23:31 +0100

igraph (0.6.5-3) unstable; urgency=low

  * Team upload.
  * Rebuild against latest gmp
  * debian/control:
     - cme fix dpkg-control
     - Canonical Vcs-Svn

 -- Andreas Tille <tille@debian.org>  Tue, 16 Jul 2013 09:12:52 +0200

igraph (0.6.5-2) unstable; urgency=low

  * Upload to sid

 -- Mathieu Malaterre <malat@debian.org>  Mon, 27 May 2013 14:01:54 +0200

igraph (0.6.5-1) experimental; urgency=low

  * New upstream
  * Bump Std-Vers to 3.9.4. No changes needed

 -- Mathieu Malaterre <malat@debian.org>  Tue, 12 Mar 2013 14:34:23 +0100

igraph (0.5.4-2) unstable; urgency=low

  * Use my @d.o alias
  * Remove DMUA flag
  * Bump Std-Vers to 3.9.3, no changes needed
  * Switch to dpkg-source 3.0 (quilt) format
  * Use dh(9), get hardening for free

 -- Mathieu Malaterre <malat@debian.org>  Wed, 11 Apr 2012 13:09:46 +0200

igraph (0.5.4-1) unstable; urgency=low

  * New upstream.

  * control: Change depends libgmp3-dev --> libgmp-dev.  Update
    Standards-Version to 3.9.1.

  * rules: empty out dependency_libs.

 -- Steve M. Robbins <smr@debian.org>  Wed, 16 Mar 2011 23:42:10 -0500

igraph (0.5.3-1) unstable; urgency=low

  * Initial upload to Debian. Closes: #546752.

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Mon, 16 Nov 2009 18:12:42 +0100
